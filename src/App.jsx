import React from 'react';

//CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

//COMPONENTS
import { Container, Col, Row } from 'reactstrap';
import Palabra from './components/Palabra';
import Lista from './components/Lista';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = { id: 0, input: '', lista: [] };

		this.handleInputChange = this.handleInputChange.bind(this);
		this.addToList = this.addToList.bind(this);
		this.clearList = this.clearList.bind(this);
		this.removeId = this.removeId.bind(this);
	}

	handleInputChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	removeId(id) {
		//Quita un elemento de la lista por id
		const lista = this.state.lista.splice(0);
		let listaSin = lista.filter((el) => el.id !== id);
		this.setState({ lista: listaSin });
	}

	addToList() {
		//Añade la palabra a la lista
		if (this.state.input !== '') {
			const id = this.state.id + 1;
			const lista = this.state.lista.splice(0);

			lista.push({ id: id, content: this.state.input });
			this.setState({ id, lista });
		}
	}

	clearList() {
		//Limpia la lista
		this.setState({ lista: [] });
	}

	render() {
		return (
			<Container>
				<h1 className="text-center display-3 my-5">
					<i className="fab fa-react mr-3" />Evaluación React
				</h1>
				<Row className="d-flex align-items-center justify-content-center">
					<Col xs="6">
						<Palabra
							handleInputChange={this.handleInputChange}
							addToList={this.addToList}
							clearList={this.clearList}
						/>
					</Col>
				</Row>
				<Row className="d-flex align-items-center justify-content-center">
					<Col xs="6">
						<Lista data={this.state.lista} removeId={this.removeId} />
					</Col>
				</Row>
			</Container>
		);
	}
}

export default App;
