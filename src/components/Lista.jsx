import React, { Component } from 'react';

import { ListGroup, ListGroupItem, Button } from 'reactstrap';

class Lista extends Component {
	render() {
		let elemsLista = this.props.data.map((el) => (
			<ListGroupItem className="d-flex justify-content-between my-1" key={el.id}>
				<span>
					<i className="fas fa-arrow-right mr-3" />
					{el.content}
				</span>
				<Button color="danger" size="sm" onClick={() => this.props.removeId(el.id)}>
					<i className="fas fa-trash-alt" />
				</Button>
			</ListGroupItem>
		));

		return <ListGroup className="my-4">{elemsLista}</ListGroup>;
	}
}

export default Lista;
