import React, { Component } from 'react';

import { InputGroup, InputGroupAddon, Input, Button } from 'reactstrap';

class Palabra extends Component {
	render() {
		return (
			<InputGroup>
				<Input name="input" onChange={this.props.handleInputChange} autoFocus />
				<InputGroupAddon addonType="append">
					<Button color="primary" onClick={this.props.addToList}>
						Afegir
					</Button>
				</InputGroupAddon>
				<InputGroupAddon addonType="append">
					<Button color="warning" onClick={this.props.clearList}>
						Reset
					</Button>
				</InputGroupAddon>
			</InputGroup>
		);
	}
}

export default Palabra;
